/*
 * uart.h
 *
 *  Created on: Oct 12, 2015
 *      Author: glpuga
 */

#ifndef MODULE_UART_H
#define MODULE_UART_H

#include "os.h"
#include "ciaak.h"


typedef struct {

	int32_t filedesc;

	int32_t baudrate;

} uartHandlerType;


void uartInit(
      uartHandlerType *uartHandler,
      const char *devName,
      int32_t baudrate);

void uartSendStringBlocking(
      uartHandlerType *uartHandler,
      const char *string);

void uartReadByteBlocking(
      uartHandlerType *uartHandler,
      uint8 *dataPtr);

void uartSendByteBlocking(
      uartHandlerType *uartHandler,
      uint8 data);

#endif /* MODULE_UART_H */
