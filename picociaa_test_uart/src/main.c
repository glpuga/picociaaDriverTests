
/*==================[inclusions]=============================================*/



#include "os.h"
#include "ciaaPOSIX_stdio.h"
#include "ciaaPOSIX_string.h"
#include "ciaak.h"

#include "uart.h"



/*==================[macros and definitions]=================================*/



#define UART_DEVICE_NAME         "/dev/serial/uart/0"
//#define UART_DEVICE_NAME         "/dev/serial/uart/1"
//#define UART_DEVICE_NAME         "/dev/serial/uart/2"
//#define UART_DEVICE_NAME         "/dev/serial/uart/3"

#define UART_DEVICE_BAUDRATE     38400

#define MAX_TEST_STRING_LEN      40

#define TASK_ACTIVATION_INTERVAL 25


#define MAX_DIGITS_IN_A_NUMBER   20



/*==================[internal data declaration]==============================*/



/*==================[internal functions declaration]=========================*/



/*==================[internal data definition]===============================*/



uartHandlerType uartHandler;

char buffer[100];



/*==================[external data definition]===============================*/



/*==================[internal functions definition]==========================*/



void printNumberOnTheConsole(int32_t number)
{
   static char charHeap[MAX_DIGITS_IN_A_NUMBER]; /* static para no usar stack */
   int32_t heapIndex;
   int32_t localNumber;
   int32_t digit;
   int32_t negativeNumber;

   localNumber = number;

   heapIndex = 0;

   if (localNumber < 0)
   {
      negativeNumber  = 1;
      localNumber = -localNumber;
   } else {
      negativeNumber  = 0;
   }

   do {
      digit      = localNumber % 10;
      localNumber = localNumber / 10;

      charHeap[heapIndex] = '0' + (char)digit;
      heapIndex++;

   } while(localNumber > 0);

   if (negativeNumber == 1)
   {
      uartSendByteBlocking(&uartHandler, '-');
   }

   while (heapIndex > 0)
   {
      heapIndex--;
      uartSendByteBlocking(&uartHandler, charHeap[heapIndex]);
   }
}



/*==================[external functions definition]==========================*/



int main(void)
{
   StartOS(AppMode1);

   return 0;
}


void ErrorHook(void)
{
   ciaaPOSIX_printf("ErrorHook was called\n");
   ciaaPOSIX_printf("Service: %d, P1: %d, P2: %d, P3: %d, RET: %d\n", OSErrorGetServiceId(), OSErrorGetParam1(), OSErrorGetParam2(), OSErrorGetParam3(), OSErrorGetRet());
   ShutdownOS(0);
}


TASK(InitTask)
{
   ciaak_start();

   uartInit(&uartHandler, UART_DEVICE_NAME, UART_DEVICE_BAUDRATE);

   uartSendStringBlocking(&uartHandler, "\r\n");
   uartSendStringBlocking(&uartHandler, "\r\n");
   uartSendStringBlocking(&uartHandler, "Test de validación del funcionamiento de drivers\r\n");
   uartSendStringBlocking(&uartHandler, "Driver UART\r\n");
   uartSendStringBlocking(&uartHandler, "\r\n");
   uartSendStringBlocking(&uartHandler, "\r\n");
   uartSendStringBlocking(&uartHandler, "Ensayo 1: Transmisión de cantidades crecientes de datos a través de la UART\r\n");
   uartSendStringBlocking(&uartHandler, "\r\n");

   ActivateTask(picociaaTest1);

   TerminateTask();
}


TASK(picociaaTest1)
{
   static int32_t testStringLen = 0;
   int32_t i;

   testStringLen++;

   for (i = 0; i < testStringLen; i++)
   {
      buffer[i] = '0' + (testStringLen % 10);
   }
   buffer[i] = '\0';

   uartSendStringBlocking(&uartHandler, buffer);
   uartSendStringBlocking(&uartHandler, "\n\r");


   if (testStringLen < MAX_TEST_STRING_LEN)
   {
      SetRelAlarm(ActivatePicociaaTest1, TASK_ACTIVATION_INTERVAL, 0);

   } else {

      uartSendStringBlocking(&uartHandler, "Ensayo 2: Repetición de los datos recibidos a través de la UART\r\n");
      uartSendStringBlocking(&uartHandler, "\r\n");

      ActivateTask(picociaaTest2);
   }

   TerminateTask();
}


TASK(picociaaTest2)
{
   char dataByte;
   int strIndex;

   strIndex = 0;

   while (1)
   {
      uartReadByteBlocking(&uartHandler, (uint8_t *)&dataByte);

      if (dataByte != '\n')
      {
         buffer[strIndex] = dataByte;
         strIndex++;
         buffer[strIndex] = 0;

      } else {

         uartSendStringBlocking(&uartHandler, "Se recibió la cadena: ");
         uartSendStringBlocking(&uartHandler, buffer);
         uartSendStringBlocking(&uartHandler, "\n\r");

         strIndex = 0;
      }
   }
}



/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

