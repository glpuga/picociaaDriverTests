
#include "uart.h"
#include "os.h"
#include "ciaaPOSIX_stdio.h"  /* <= device handler header */
#include "ciaaPOSIX_string.h" /* <= string header */
#include "ciaak.h"



void uartInit(uartHandlerType *uartHandler, const char *devName, int32_t baudrate)
{
   uartHandler->filedesc = ciaaPOSIX_open(devName, ciaaPOSIX_O_RDWR);

   uartHandler->baudrate = baudrate;

   ciaaPOSIX_ioctl(uartHandler->filedesc, ciaaPOSIX_IOCTL_SET_BAUDRATE, (void *)baudrate);

}


void uartSendStringBlocking(uartHandlerType *uartHandler, const char *string)
{
   int32_t retValue, stringLen;

   stringLen = ciaaPOSIX_strlen(string);

   retValue = ciaaPOSIX_write(uartHandler->filedesc, string, stringLen);

   if (retValue < stringLen)
   {
      ShutdownOS(1);
   }
}


void uartReadByteBlocking(uartHandlerType *uartHandler, uint8 *dataPtr)
{
   int32_t retValue;

   retValue = ciaaPOSIX_read(uartHandler->filedesc, (void *)dataPtr, 1);

   if (retValue <= 0)
   {
      ShutdownOS(1);
   }
}


void uartSendByteBlocking(uartHandlerType *uartHandler, uint8 data)
{
   int32_t retValue;

   retValue = ciaaPOSIX_write(uartHandler->filedesc, (void *)&data, 1);

   if (retValue <= 0)
   {
      ShutdownOS(1);
   }
}


